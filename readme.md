[![build status](https://gitlab.com/robertomorati/owlcustomers/badges/master/pipeline.svg)](https://gitlab.com/robertomorati/owlcustomers/commits/master)
[![coverage report](https://gitlab.com/robertomorati/owlcustomers/badges/master/coverage.svg)](https://gitlab.com/robertomorati/owlcustomers/commits/master)
<br />

#### OWL Customers

**Attention:**
Before the next steps.
In order to make the setup of GMaps API V3 simpler, was decide to use the key inside of settings. So, find the variable GOOGLE_MAPS_KEY_API and set up your key. Or you can use my temporary key.

### Installing

  There is a simple documentation with pages:
  https://robertomorati.gitlab.io/owlcustomers/

## 1. Clone the project

	git clone https://gitlab.com/robertomorati/owlcustomers.git

## 2. Install OS dependencies

    sudo xargs apt-get install -y < requirements/debian/base.txt
    sudo xargs apt-get install -y < requirements/debian/doc.txt

## 2. Create virtual env

    python3 -m venv /env/owlcustomers

## 4. Install dependencies on virtual env

    source /path/to/env/owlcustomers/bin/activate
    pip3 install -r requirements/test-requirements.txt
    
## 5. Set up database

Copy the  local.example.py to local.py and, set up the database settings.

    # Optional: compose with mysql and phpmyadmin
    sudo docker-compose -f docker/db-docker-compose.yaml up -d

You can set up the file local.py with data from db-docker-compose.yaml or set up MySQL by yourself.
Btw, in the end of settings.py, uncomment the "include with optional". These lines allows the use of local.py.

Credentials to acess phpmyadmin:
     http://localhost:8090/
     user: root
     pwd: owlcustomers
     
     # migrate the database and load the customers   
    ./manage.py  migrate
    ./manage.py  load_customers

## 6 - Admin acess

    email (user): admin@admin.com
    password: admin


### Running with docker
## 1. Docker

Also, you can run the app OWL Customers:

    sudo docker-compose -f docker/dev-docker-compose.yaml up --build

## 2. API

    API (try out):
    http://localhost:8088/api/swagger/

    API Doc:
    http://localhost:8088/api/redoc/

## 3. Postman

    Postman collection
    https://www.getpostman.com/collections/7bc0519f0f2c27129414

## 4. Queries for GraphQL

Retrieve all customers:
   
    query{
      allCustomers{
        id,
        idRef,
        firstName,
        lastName,
        city{
          name,
          state{
            abbreviation,
            name
          },
          point,
        }
        title{
          name
        },
        company{
          name
        }
      }
    }
   

Retrive customer by id:
    
    query{
      customer(customerId:1){
        id,
        idRef,
        firstName,
        lastName,
        city{
          name,
          state{
            abbreviation,
            name
          },
          point,
        }
        title{
          name
        },
        company{
          name
        }
      }
    }
  
