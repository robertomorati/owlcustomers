# App Customers

App Customers allows the registration of customers, title, city and state.

### UML Model

![UML Model Customers](./customers.png)


::: owlcustomers.apps.customers.models
    rendering:
      show_root_full_path: true
      show_root_members_full_path: true
      show_root_heading: true

::: owlcustomers.apps.customers.enums
    rendering:
      show_root_full_path: true
      show_root_members_full_path: true
      show_root_heading: true