"""
Module to perform data for testing
"""
from django.test import TestCase

from owlcustomers.apps.customers.models import (
    Title,
    City,
    State,
    Company,
    Customer,
)


class OWLCustomersAppTests(TestCase):
    """
    Perfom data for tests
    """

    @classmethod
    def get_title(cls, name) -> Title:
        """
        retrieve title
        """
        title = Title.objects.create(name=name)
        return title

    @classmethod
    def get_company(cls, name) -> Company:
        """
        retrieve company
        """
        company = Company.objects.create(name=name)
        return company

    @classmethod
    def get_state(cls, name, abbreviation) -> State:
        """
        retrieve state
        """
        state = State.objects.create(name=name, abbreviation=abbreviation)
        return state

    @classmethod
    def get_city(cls, name, state, point) -> City:
        """
        retrieve city
        """

        name = "Vitoria"
        city = City.objects.create(name=name, state=state, point=point)
        return city

    # pylint: disable=too-many-arguments
    @classmethod
    def get_customer(cls, email, first_name, last_name, city, company) -> Customer:
        """
        retrieve customer
        """
        customer = Customer.objects.create(
            id_ref=1,
            email=email,
            first_name=first_name,
            last_name=last_name,
            gender=1,
            city=city,
            company=company,
        )
        return customer
