"""
Command for generate UML model
"""
from django.core.management.base import BaseCommand
from django.core.management import call_command
from django.conf import settings


# pylint: disable=invalid-name
# pylint: disable=no-self-use
class Command(BaseCommand):
    """
    Command for generate UML model
    """

    def get_diretorio(self, nome_app):
        """
        retrieve the directory to save the image
        """
        return ("%s/docs/%s/docs/%s.png") % (settings.BASE_DIR, nome_app, nome_app)

    def handle(self, *args, **options):
        apps = [
            app
            for app in settings.INSTALLED_APPS
            if app.startswith("owlcustomers.apps.") and not app.endswith((".account",))
        ]

        for app in apps:
            nome_app = app.split(".")[2]
            args = [
                "--inheritance",
                "--output",
                self.get_diretorio(nome_app),
                "--arrow-shape",
                "diamond",
                nome_app,
            ]
            call_command("graph_models", *args)
