"""
Command to load custumers
"""
from django.core.management.base import BaseCommand
from django.contrib.gis.geos import Point
from django.conf import settings
from geopy.geocoders import Nominatim
import pandas as pd
import requests

from owlcustomers.apps.customers.models import (
    Customer,
    State,
    Title,
    Company,
    City,
)
from owlcustomers.apps.customers.enums import Gender


class Command(BaseCommand):
    """
    Command to load custumers
    """

    help = "Load custumers from csv file"

    def add_arguments(self, parser):
        parser.add_argument(
            "file_name",
            type=str,
            nargs="?",
            default="customers.csv",
            help="Indicate the file name",
        )

    @classmethod
    def get_subset(cls, data, subset):
        """
        return subset of data
        """
        return list(set(data[subset].values))

    @classmethod
    def get_point(cls, city):
        """
        retrieve latitude and longitude of city
        also, create logs about address issues
        """
        geolocator = Nominatim(user_agent="owscustomers")

        # retrieve from maps
        req = requests.get(
            "https://maps.googleapis.com/maps/api/geocode/json?address="
            + city
            + "&key="
            + settings.GOOGLE_MAPS_KEY_API
        )
        if req.status_code == 200:
            res = req.json()
            if res["status"] == "OK":
                location = res["results"][0]["geometry"]["location"]
                return Point(
                    location["lat"],
                    location["lng"],
                )

            # retrieve open street map
            location = geolocator.geocode(city)
            if location:
                return Point(location.latitude, location.longitude)
        return Point(0, 0)

    def load_customers(self, data):
        """
        load customers
        """
        states = State.objects.all()

        # load titles
        Title.objects.bulk_create(
            [Title(name=t) for t in self.get_subset(data, "title")],
            ignore_conflicts=True,
        )

        # load companies
        Company.objects.bulk_create(
            [Company(name=t) for t in self.get_subset(data, "company")],
            ignore_conflicts=True,
        )

        # load cities
        cities = [
            City(
                name=c.split(",")[0],
                point=self.get_point(c),
                state_id=states.get(abbreviation=c.split(",")[1].strip()).id,
            )
            for c in self.get_subset(data, "city")
        ]

        City.objects.bulk_create(cities, ignore_conflicts=True)

        titles = Title.objects.all()
        companies = Company.objects.all()
        cities = City.objects.all()

        # load customers
        customers = []

        # pylint: disable=unused-variable
        for key, value in data.iterrows():
            customer = Customer(
                id_ref=int(value["id"]),
                email=value["email"],
                first_name=value["first_name"],
                last_name=value["last_name"] if value["last_name"] else "",
                gender=Gender.get_key(value["gender"]),
                title=titles.filter(name=value["title"]).first(),
                city=cities.filter(name=value["city"].split(",")[0]).first(),
                company=companies.filter(name=value["company"]).first(),
            )
            customers.append(customer)

        Customer.objects.bulk_create(customers, ignore_conflicts=True)

    def handle(self, *args, **kwargs):
        """
        handle
        """

        file_path = str(settings.BASE_DIR) + "/"
        file_name = str(kwargs["file_name"])
        if file_name:
            file_path = file_path + file_name
            data = pd.read_csv(file_path)
            self.load_customers(data)
