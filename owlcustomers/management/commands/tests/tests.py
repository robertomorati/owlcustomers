"""
Testing command load_users
"""
from django.core.management import call_command
from django.test import TestCase

from owlcustomers.apps.customers.models import Customer


class CommandsTestCase(TestCase):
    """
    Test custom command
    """

    def test_load_customers(self):
        """
        Test load customers command
        """
        args = []
        opts = {}
        call_command("load_customers", *args, **opts)
        self.assertEqual(Customer.objects.all().count(), 1000)
