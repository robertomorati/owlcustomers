"""
App Customers
"""

from django.apps import AppConfig


class CustomersConfig(AppConfig):
    """
    Class with App Customers settings
    """

    name = "owlcustomers.apps.customers"
