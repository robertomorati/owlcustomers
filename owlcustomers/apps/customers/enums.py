"""
Enums classes
"""
from django.db import models
from django.utils.translation import gettext_lazy as _


class Gender(models.IntegerChoices):
    """
    Enum for gender options
    """

    MALE = 1, _("Male")
    FEMALE = 2, _("Female")

    @staticmethod
    def get_keys():
        """
        get gender keys
        """
        # pylint: disable=not-an-iterable
        choices = Gender.choices
        return [key for key, value in choices]

    @staticmethod
    def get_value(key):
        """
        get gender value by key
        """
        # pylint: disable=not-an-iterable
        choices = Gender.choices
        return [v for (k, v) in choices if k == key][0]

    @staticmethod
    def get_key(value):
        """
        get gender key by value
        """
        # pylint: disable=not-an-iterable
        choices = Gender.choices
        return [k for (k, v) in choices if v == value][0]
