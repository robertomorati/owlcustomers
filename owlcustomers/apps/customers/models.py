# -*- coding: utf-8 -*-
"""
Models for customers app
"""
from django.contrib.gis.db import models
from django.utils.translation import ugettext_lazy as _

from owlcustomers.apps.customers.enums import Gender


class Company(models.Model):
    """
    Represents a company
    """

    name = models.CharField(verbose_name=_("Name"), max_length=50, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("company")
        verbose_name_plural = _("companies")


class State(models.Model):
    """
    Represents a state
    """

    abbreviation = models.CharField(verbose_name=_("Name"), max_length=2, unique=True)
    name = models.CharField(verbose_name=_("Name"), max_length=50)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("state")
        verbose_name_plural = _("states")


class City(models.Model):
    """
    Represents a city
    """

    name = models.CharField(verbose_name=_("Name"), max_length=50, unique=True)
    point = models.PointField(verbose_name=_("Point"), default=None)  # lat and long
    state = models.ForeignKey(
        to=State,
        on_delete=models.CASCADE,
        related_name="states",
        verbose_name=_("State"),
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("city")
        verbose_name_plural = _("cities")


class Title(models.Model):
    """
    Represents a title (position)
    """

    name = models.CharField(verbose_name=_("Name"), max_length=50, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("title")
        verbose_name_plural = _("titles")


class Customer(models.Model):
    """
    Represents a customer
    """

    id_ref = models.IntegerField(
        verbose_name=_("Id Ref"),
        blank=True,
        null=True,
    )
    email = models.EmailField(_("Email"), unique=True)
    first_name = models.CharField(_("First name"), max_length=25)
    last_name = models.CharField(_("Last name"), max_length=50)
    gender = models.PositiveSmallIntegerField(
        _("Gender"),
        choices=Gender.choices,
    )
    title = models.ForeignKey(
        to=Title,
        on_delete=models.CASCADE,
        related_name="titles",
        verbose_name=_("Title"),
        blank=True,
        null=True,
    )
    city = models.ForeignKey(
        to=City,
        on_delete=models.CASCADE,
        related_name="cities",
        verbose_name=_("City"),
    )
    company = models.ForeignKey(
        to=Company,
        on_delete=models.CASCADE,
        related_name="companies",
        verbose_name=_("Company"),
    )

    def __str__(self):
        return ("%s %s") % (self.first_name, self.last_name)

    class Meta:
        verbose_name = _("customer")
        verbose_name_plural = _("customers")


# class LogDataImport(models.Model):
#    """
#    Log to help in correction of data imported
#    """
#
#    reference_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
#    reference_id = models.PositiveIntegerField()
#    reference = GenericForeignKey("reference_type", "reference_id")
#    data = models.CharField(_("Data"), max_length=500)
#
#    def __str__(self):
#        return self.data
#
#    class Meta:
#        verbose_name = _("data")
#        verbose_name_plural = _("data")
