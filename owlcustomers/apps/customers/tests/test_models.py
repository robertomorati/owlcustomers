"""
Testing for App Customers models
"""
from django.contrib.gis.geos import Point

from owlcustomers.tests.test import OWLCustomersAppTests


class CustomersManagerTestCase(OWLCustomersAppTests):
    """
    Testings fors users with CustomersManager
    """

    def setUp(self):
        self.point = Point(0, 0)

    def test_create_title(self):
        """
        Testing create title
        """
        name = "Title"
        title = self.get_title(name)
        self.assertEqual(title.__str__(), name)

    def test_create_state_city(self):
        """
        Testing create state and city
        """

        # testing state
        abbreviation = "ES"
        name = "Espirito Santo"
        state = self.get_state(name=name, abbreviation=abbreviation)
        self.assertEqual(state.abbreviation, abbreviation)
        self.assertEqual(state.__str__(), name)

        # testing city
        name = "Vitoria"
        city = self.get_city(name=name, state=state, point=self.point)
        self.assertEqual(city.state.name, state.name)
        self.assertEqual(city.__str__(), name)

    def test_create_company(self):
        """
        Testing create company
        """
        name = "Company"
        company = self.get_company(name=name)
        self.assertEqual(company.__str__(), name)

    def test_create_customer(self):
        """
        Testing create customer
        """
        company = self.get_company(name="Company")
        state = self.get_state(name="Espirito Santo", abbreviation="ES")
        city = self.get_city(name="City", state=state, point=self.point)

        customer = self.get_customer(
            email="rm@email.com",
            first_name="Roberto",
            last_name="Morati",
            city=city,
            company=company,
        )
        self.assertEqual(customer.email, "rm@email.com")
        self.assertEqual(customer.__str__(), "Roberto Morati")

    # def test_create_log(self):
    #    """
    #    Testing create log
    #    """
    #    data = "Log about import"
    #    state = State.objects.create(name="State")
    #    city = City.objects.create(name="City", state=state, point=self.point)
    #
    #    log = LogDataImport.objects.create(data=data, reference=city)
    #    self.assertEqual(log.__str__(), data)
    #
