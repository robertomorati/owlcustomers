"""
Testing for App Customers enums
"""
from django.test import TestCase

from owlcustomers.apps.customers.enums import Gender


class CustomersManagerTestCase(TestCase):
    """
    Testings enum Gender
    """

    def test_enum_gender(self):
        """
        Testing enum gender
        """
        key = Gender.get_key("Male")
        self.assertEqual(key, 1)

        value = Gender.get_value(1)
        self.assertEqual(value, "Male")

        keys = Gender.get_keys()
        self.assertEqual(len(keys), 2)
