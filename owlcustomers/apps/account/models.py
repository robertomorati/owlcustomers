"""
Module with App Account models
"""

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import (
    AbstractUser,
    Group as DefaultGroup,
    UserManager as BaseUserManager,
)


class UserManager(BaseUserManager):
    """
    Manager for user management.
    Override default management to use email as username.
    """

    def _create_user(self, username, email, password, **extra_fields):
        """
        Create new user.
        """
        email = self.normalize_email(email)
        user = self.model(**extra_fields)
        user.email = email
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email=None, password=None, **extra_fields):
        """
        Create a non-admin user
        """
        # pylint: disable=arguments-differ
        return super().create_user(None, email, password, **extra_fields)

    def create_superuser(self, email=None, password=None, **extra_fields):
        """
        Create admin user
        """
        # pylint: disable=arguments-differ
        return super().create_superuser(None, email, password, **extra_fields)


class User(AbstractUser):
    """
    class that represents an User
    """

    username = None  # remove username
    email = models.EmailField(_("email address"), unique=True)

    objects = UserManager()

    def get_full_name(self):
        return "%s %s" % (self.first_name, self.last_name)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = [
        "first_name",
    ]


class Group(DefaultGroup):
    """
    Proxy class for default group.
    """

    class Meta:
        verbose_name = _("group")
        verbose_name_plural = _("groups")
        proxy = True
