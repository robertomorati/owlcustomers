from django.core.management import call_command
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("account", "0001_initial"),
    ]

    def load_data(apps, schema_editor):
        call_command("loaddata", "account_initial_data.json")

    operations = [
        migrations.RunPython(load_data),
    ]
