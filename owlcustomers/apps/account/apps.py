"""
App Account Settings
"""

from django.apps import AppConfig


class AccountConfig(AppConfig):
    """
    Class with App Account settings
    """

    name = "owlcustomers.apps.account"
