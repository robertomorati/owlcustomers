"""
Testing for App Account settings
"""
from django.apps import apps
from django.test import TestCase

from owlcustomers.apps.account.apps import AccountConfig


class AccountConfigTest(TestCase):
    """
    Testing module name
    """

    def test_apps(self):
        """
        Testing module name
        """
        self.assertEqual(AccountConfig.name, "owlcustomers.apps.account")
        self.assertEqual(
            apps.get_app_config("account").name, "owlcustomers.apps.account"
        )
