"""
Testing for App Account models
"""

from django.db.models.signals import post_save
from django.test import TestCase

from owlcustomers.apps.account.models import User


class UserManagerTestCase(TestCase):
    """
    Testings fprs users with UserManager
    """

    def post_save_listener(self, *_args, **_kwargs):
        """
        Listener for signals count after save an entity
        """
        self.signals_count += 1

    def setUp(self):
        self.signals_count = 0
        post_save.connect(self.post_save_listener, sender=User)

    def test_create_user(self):
        """
        Testing create new users
        """
        email_lowercase = "user@example.com"
        user = User.objects.create_user(email_lowercase)
        self.assertEqual(user.email, email_lowercase)

    def test_create_superuser(self):
        """
        Testing create super user (admin)
        """
        User.objects.create_superuser("superuser@example.com", "1")
        self.assertEqual(self.signals_count, 1)

    def test_user_fullname(self):
        """
        Testing def for get full name
        """
        first_name = "First"
        last_name = "Second"
        full_name = first_name + " " + last_name
        user = User()
        user.first_name = first_name
        user.last_name = last_name
        self.assertEqual(user.get_full_name(), full_name)
