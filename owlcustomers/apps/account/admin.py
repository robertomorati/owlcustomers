"""
Admin for App Acoount
"""
from django.contrib import admin
from django.contrib.auth import admin as auth_admin
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group as DefaultGroup
from django.utils.translation import gettext_lazy as _

from owlcustomers.apps.account.models import Group

USER_MODEL = get_user_model()


@admin.register(USER_MODEL)
class UserAdmin(auth_admin.UserAdmin):
    """
    Admin users
    """

    model = USER_MODEL
    list_display = ("email", "first_name", "last_name", "is_staff")
    search_fields = (
        "email",
        "first_name",
        "last_name",
    )
    ordering = ("email",)
    fieldsets = (
        (_("Account info"), {"fields": ("email", "password")}),
        (
            _("Personal info"),
            {
                "fields": (
                    "first_name",
                    "last_name",
                )
            },
        ),
        (
            _("Permissions"),
            {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                ),
            },
        ),
        (_("Important dates"), {"fields": ("last_login", "date_joined")}),
    )
    add_fieldsets = (
        (_("Account"), {"fields": ("email", "password1", "password2")}),
        (
            _("Personal info"),
            {
                "fields": (
                    "first_name",
                    "last_name",
                )
            },
        ),
    )


admin.site.unregister(DefaultGroup)


@admin.register(Group)
class GroupAdmin(auth_admin.GroupAdmin):
    """
    Groups administration
    Works as an Proxy for Default Group (Django)
    """

    model = Group
