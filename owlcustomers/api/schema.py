"""
Schema for graphene
"""
# pylint: disable=unused-import, missing-class-docstring, too-few-public-methods
import graphene
from graphene_django.types import DjangoObjectType
from graphene_gis.converter import gis_converter  # noqa

from owlcustomers.apps.customers import models as models_customers


class TitleType(DjangoObjectType):
    """
    Creates a graphql type for model title
    """

    class Meta:

        model = models_customers.Title
        fields = ("name",)


class StateType(DjangoObjectType):
    """
    Creates a graphql type for model state
    """

    class Meta:

        model = models_customers.State
        fields = (
            "name",
            "abbreviation",
        )


class CityType(DjangoObjectType):
    """
    Creates a graphql type for model city
    """

    class Meta:

        model = models_customers.City
        fields = "__all__"


class CompanyType(DjangoObjectType):
    """
    Creates a graphql type for model company
    """

    class Meta:

        model = models_customers.Company
        fields = ("name",)


class CustomerType(DjangoObjectType):
    """
    Creates a graphql type for model customers
    """

    class Meta:
        model = models_customers.Customer
        fields = "__all__"


class Query(graphene.ObjectType):
    """
    Query graphene
    """

    all_customers = graphene.List(CustomerType)
    customer = graphene.Field(CustomerType, customer_id=graphene.Int())

    # pylint: disable=no-self-use, unused-argument
    def resolve_all_customers(self, info):
        """
        retrieve all customers
        """
        return models_customers.Customer.objects.all()

    # pylint: disable=no-self-use, unused-argument
    def resolve_customer(self, info, customer_id):
        """
        retrieve customer by id
        """
        return models_customers.Customer.objects.get(pk=customer_id)


schema = graphene.Schema(query=Query)
