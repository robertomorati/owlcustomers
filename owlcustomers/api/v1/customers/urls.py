# -*- coding: utf-8 -*-
"""
URLs API Customers - version 1.0
"""

from django.urls import path
from owlcustomers.api.v1.customers import views

app_name = "customers"


urlpatterns = [
    path(
        "list/",
        views.CustomerListView.as_view(),
        name="customer_list",
    ),
    path(
        "<int:pk>/",
        views.CustomerRetrieveAPIView.as_view(),
        name="customer_get",
    ),
]
