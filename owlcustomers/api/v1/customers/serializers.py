# -*- coding: utf-8 -*-
"""
Serializers for customers app
"""
from rest_framework import serializers
from owlcustomers.apps.customers.enums import Gender
from owlcustomers.apps.customers.models import City, State, Company, Title, Customer


class StateSerializer(serializers.ModelSerializer):
    """
    Seriaizer for state model
    """

    class Meta:
        model = State
        fields = ["name", "abbreviation"]


class CitySerializer(serializers.ModelSerializer):
    """
    Seriaizer for city model
    """

    state = StateSerializer(many=False)

    class Meta:
        model = City
        fields = [
            "name",
            "state",
            "point",
        ]


class CompanySerializer(serializers.ModelSerializer):
    """
    Seriaizer for Company model
    """

    class Meta:
        model = Company
        fields = [
            "name",
        ]


class TitleSerializer(serializers.ModelSerializer):
    """
    Seriaizer for Title model
    """

    class Meta:
        model = Title
        fields = [
            "name",
        ]


class CustomerSerializer(serializers.ModelSerializer):
    """
    Seriaizer for customer model
    """

    city = CitySerializer(many=False)
    title = TitleSerializer(many=False)
    gender = serializers.SerializerMethodField()

    # pylint: disable=no-self-use
    def get_gender(self, obj):
        """
        get value gender
        """
        return Gender.get_value(obj.gender)

    class Meta:
        model = Customer
        fields = [
            "id",
            "id_ref",
            "email",
            "first_name",
            "last_name",
            "gender",
            "title",
            "city",
        ]
