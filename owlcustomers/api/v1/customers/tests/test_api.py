"""
Testing API V1
"""
from django.urls import reverse
from django.contrib.gis.geos import Point
from django.test.client import RequestFactory
from rest_framework import status
from rest_framework.test import APITestCase, APIClient

from owlcustomers.tests.test import OWLCustomersAppTests


class CustomersAPITests(APITestCase, OWLCustomersAppTests):
    """
    Provide testes for Customers API
    """

    def setUp(self):
        """
        setUp Test
        """
        self.client = APIClient()
        self.factory = RequestFactory()

        first_name = "Roberto"
        last_name = "Morati"
        email = "rmjr@email.com"

        self.company = self.get_company(name="Company")
        self.state = self.get_state(name="State", abbreviation="ST")
        self.city = self.get_city(name="City", state=self.state, point=Point(0, 0))

        self.customer = self.get_customer(
            email=email,
            first_name=first_name,
            last_name=last_name,
            city=self.city,
            company=self.company,
        )

        # retrieve token
        url = reverse("api:v1:token_obtain_pair")
        response = self.client.post(
            url,
            {"email": "admin@admin.com", "password": "admin"},
            format="json",
        )
        self.token = response.data["access"]
        self.client.credentials(HTTP_AUTHORIZATION="Bearer " + self.token)

    def test_list_customer(self):
        """
        Testings customers list
        """
        url = reverse("api:v1:customers:customer_list")
        response = self.client.get(
            url,
            headers={"Authorization": "Bearer " + "dsadas"},
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
