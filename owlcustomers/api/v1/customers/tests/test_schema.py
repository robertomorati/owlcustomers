"""
Testing Schema GaphQL
"""

import json
from django.test import Client
from django.contrib.gis.geos import Point
from graphene_django.utils.testing import GraphQLTestCase

from owlcustomers.tests.test import OWLCustomersAppTests

class CustomersGraphQLTestCase(OWLCustomersAppTests, GraphQLTestCase):
    """
    customers for testing graphql queries
    """

    def setUp(self):
        """
        setUp Test
        """

        first_name = "Roberto"
        last_name = "Morati"
        email = "rmjr@example.com"

        self.company = self.get_company(name="Company")
        self.state = self.get_state(name="Espirito Santo", abbreviation="ES")
        self.city = self.get_city(name="Vitoria", state=self.state, point=Point(0, 0))

        self.customer = self.get_customer(
            email=email,
            first_name=first_name,
            last_name=last_name,
            city=self.city,
            company=self.company,
        )

    def test_retrieve_all_customers(self):
        """
        retrieve all customers
        """
        query = """
            query {
              allCustomers{
                id,
                idRef,
                firstName,
                lastName,
                city{
                  name,
                  state{
                    abbreviation,
                    name
                  },
                  point
                }
                title{
                  name
                },
                company{
                  name
                }
              }
            }
            """

        body = {"query": query}
        response = Client().post(
            "/graphql/#", json.dumps(body), content_type="application/json"
        )
        self.assertEqual(response.status_code, 200)

    def test_retrieve_customer(self):
        """
        retrieve customer
        """
        query = """
            query {
              customer(customerId: 1){
                id,
                idRef,
                firstName,
                lastName,
                city{
                  name,
                  state{
                    abbreviation,
                    name
                  },
                  point,
                }
                title{
                  name
                },
                company{
                  name
                }
              }
            }
            """

        body = {"query": query}
        body["op_name"] = "customer"

        response = Client().post(
            "/graphql/#", json.dumps(body), content_type="application/json"
        )

        self.assertEqual(response.status_code, 200)
