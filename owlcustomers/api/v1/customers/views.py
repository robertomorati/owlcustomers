# -*- coding: utf-8 -*-
"""
Views for customers api.
"""
from rest_framework import generics
from rest_framework.permissions import AllowAny

from owlcustomers.apps.customers.models import Customer
from owlcustomers.api.v1.customers.serializers import CustomerSerializer


class CustomerListView(generics.ListAPIView):
    """
    Retrieve a list of all customers.
    """

    serializer_class = CustomerSerializer
    queryset = Customer.objects.all()
    permission_classes = [AllowAny]


class CustomerRetrieveAPIView(generics.RetrieveAPIView):
    """
    Retrieves a customer by the id if exist
    """

    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
    permission_classes = [AllowAny]
