"""
Local settings example.
Copy this file to local.py and change settings
"""

import debug_toolbar
from django.urls import include, path

DEBUG = True

SECRET_KEY = "supersecretkey"

INSTALLED_APPS += [
    "debug_toolbar",
]

MIDDLEWARE += [
    "debug_toolbar.middleware.DebugToolbarMiddleware",
]

INTERNAL_IPS = [
    "127.0.0.1",
]

ENABLE_DEBUG_TOOLBAR = True

EXTRA_URLS = [
    path("__debug__/", include(debug_toolbar.urls)),
]

# TODO: update with your data
DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.mysql', 
        'NAME': 'owlcustomers',
        'USER': '',
        'PASSWORD': '',
        'HOST': '', 
        'PORT': '',
    }
}