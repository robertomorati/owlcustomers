#!/bin/sh

export DB_ADAPTER="mysql"
export DB_HOST="owl-db"
export DB_PORT="3306"
export DB_NAME="owlcustomers"
export DB_USER="owlcustomers"
export DB_PASS="owlcustomers"
export DEBUG_APP="True"

wget https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh -O /bin/wait-for-it &&
        chmod +x /bin/wait-for-it

/bin/wait-for-it --strict -h owl-db -p 3306 -t 0

python manage.py collectstatic --noinput
python manage.py migrate --noinput
python manage.py load_customers "customers.csv"
exec "$@"
